import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LandingPageComponent} from './landing-page/landing-page.component';
import {NavigationBarComponent} from './landing-page/navigation-bar/navigation-bar.component';
import {MatButtonModule} from '@angular/material/button';
import {FeaturesComponent} from './landing-page/features/features.component';
import {CoreRoutingModule} from './core-routing.module';
import {RouterModule} from '@angular/router';
import {LogInComponent} from './landing-page/log-in/log-in.component';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {SignUpComponent} from './landing-page/sign-up/sign-up.component';
import {ContactComponent} from './landing-page/contact/contact.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';

@NgModule({
  declarations: [LandingPageComponent, NavigationBarComponent, FeaturesComponent, LogInComponent, SignUpComponent, ContactComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    CoreRoutingModule,
    RouterModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule
  ]
})
export class CoreModule {}
