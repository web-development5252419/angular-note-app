import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationSidebarComponent} from './navigation-sidebar.component';
import {NotesRoutingModule} from '../notes/notes-routing.module';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {GetInitialsPipe} from '../shared/services/getInitials.pipe';

@NgModule({
  declarations: [NavigationSidebarComponent, GetInitialsPipe],
  exports: [NavigationSidebarComponent],
  imports: [CommonModule, NotesRoutingModule, MatIconModule, MatTooltipModule]
})
export class NavigationSidebarModule {}
