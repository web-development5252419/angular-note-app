export interface Note {
  id: string;
  userId: string;
  title: string;
  content: string;
  color?: string;
  startDate?: string;
  endDate?: string;
}
