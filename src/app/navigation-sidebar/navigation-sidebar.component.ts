import {Component, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth/auth.service';

@Component({
  selector: 'app-navigation-sidebar',
  templateUrl: './navigation-sidebar.component.html',
  styleUrls: ['./navigation-sidebar.component.scss']
})
export class NavigationSidebarComponent implements OnInit {
  constructor(public auth: AuthService) {}

  ngOnInit(): void {}

  logOut() {
    this.auth.logout();
  }
}
