<div align="center">
<h1>
Angular Note App<br/>
https://tatarualexandru777.gitlab.io/angular-note-app <br/>
</h1>
</div>

<h1 align="center">DESCRIPTION</h1>
<p align="center">This Web Application was built using Angular 13, you can write a summary of a certain subject or write down certain thoughts and events to keep a journal of them, or set certain goals which helps you trigger new behaviours, guides your focus and helps you sustain that momentum in life.

- The user can create post-it notes or a to-do list in order to keep track of goals, subjects or events.
- Implemented user login and signup with Google and email. Also, user authentication is required in order to access the notes and goals.
- User accounts, goals and notes data are accessed and stored using Firebase.
- Implemented a contact section where the user can send suggestions or bugs via the application to my email.

</p>
<h1 align="center">BUILT WITH</h1>
 <p align="center">WebStorm(IDE), GitLab, Angular 13, Angular Material, TypeScript, HTML5, SCSS, Firebase</p>

<h1 align="center">INSTALLATION</h1>
<p align="center">The application is not currently available for public release.</p>
<h1 align="center">SCREENSHOTS</h1>
<img  src="https://i.imgur.com/axuMIau.png">
<img src="https://i.imgur.com/eiQBYFa.png" align="center">
<img src="https://i.imgur.com/whKnZDu.png" align="center">

<h1 align="center">OBSERVATIONS</h1>
<p align="center">The code has no comments at the moment</p>
