import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotesComponent} from './notes.component';
import {AuthGuardService} from '../shared/services/auth/auth-guard.service';

const noteRoutes: Routes = [
  {
    path: 'notes',
    component: NotesComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(noteRoutes)],
  exports: [RouterModule]
})
export class NotesRoutingModule {}
