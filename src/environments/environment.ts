// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'angular-note-app-6af0b',
    appId: '1:138024621788:web:e46be7bfe4fa4b3724c619',
    databaseURL: 'https://angular-note-app-6af0b-default-rtdb.europe-west1.firebasedatabase.app',
    storageBucket: 'angular-note-app-6af0b.appspot.com',
    apiKey: 'AIzaSyC_mwAbay2XXjBPxcxLuKk7XWKmjvrbzh0',
    authDomain: 'angular-note-app-6af0b.firebaseapp.com',
    messagingSenderId: '138024621788',
    measurementId: 'G-TGYQ87X6X2',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
