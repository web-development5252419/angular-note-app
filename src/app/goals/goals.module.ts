import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GoalsComponent} from './goals.component';
import {GoalsRoutingModule} from './goals-routing.module';
import {NavigationSidebarModule} from '../navigation-sidebar/navigation-sidebar.module';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  declarations: [GoalsComponent],
  imports: [
    CommonModule,
    GoalsRoutingModule,
    NavigationSidebarModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCheckboxModule
  ]
})
export class GoalsModule {}
