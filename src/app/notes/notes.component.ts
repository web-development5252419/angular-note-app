import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Note} from './note.interface';
import {FirestoreEntity} from '../shared/entities/firestore.entity';
import {DialogComponent} from '../shared/components/dialog/dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {AuthService} from '../shared/services/auth/auth.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {
  addNoteForm: FormGroup;
  notes: Note[] = [];

  @Input() dialogForm: FormGroup;

  constructor(private firestoreEntity: FirestoreEntity, private dialog: MatDialog, private authService: AuthService) {}

  ngOnInit(): void {
    this.getNotes();
    this.addNoteForm = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.maxLength(255)]),
      content: new FormControl('', [Validators.required]),
      startDate: new FormControl(''),
      endDate: new FormControl('')
    });
  }

  addNote() {
    const note = JSON.parse(JSON.stringify(this.addNoteForm.value));
    note.userId = this.authService.firebaseUser.uid;
    this.firestoreEntity.addDocument<Note>('notes', note).subscribe(newNote => {
      note.id = newNote.id; // id from firebase same as the one when created a note
      this.notes.push(note);
      this.addNoteForm.reset();
    });
  }

  editNote(selectedNote) {
    this.dialog
      .open(DialogComponent, {
        data: selectedNote,
        width: '500px'
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          result.userId = this.authService.firebaseUser.uid;
          this.firestoreEntity.updateDocument<Note>('notes', selectedNote.id, result).subscribe(() => {
            const noteIndex = this.notes.findIndex(note => note.id === selectedNote.id);
            result.id = selectedNote.id;
            this.notes[noteIndex] = result;
          });
        }
      });
  }

  deleteNote(id: string) {
    this.firestoreEntity.deleteDocument<Note>('notes', id).subscribe(() => {
      const noteIndex = this.notes.findIndex(note => note.id === id);
      this.notes.splice(noteIndex, 1);
    });
  }

  getNotes() {
    this.firestoreEntity
      .getFilteredCollection<Note[]>('notes', [
        {
          key: 'userId',
          operator: '==',
          value: this.authService.firebaseUser.uid
        }
      ])
      .subscribe(notes => {
        this.notes = notes;
      });
  }
}
