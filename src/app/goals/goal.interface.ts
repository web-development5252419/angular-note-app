export interface Goal {
  id: string;
  userId: string;
  title: string;
  content: {
    value: string;
    isChecked: boolean;
  }[];
  color?: string;
  startDate?: string;
  endDate?: string;
}
